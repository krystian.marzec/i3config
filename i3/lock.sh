#!/bin/sh

B='#00000000'  # blank
C='#ffffff22'  # clear ish
D='#40bf40ff'  # default
T='#ee00eeee'  # text
W='#880000bb'  # wrong
V='#bb00bbbb'  # verifying
A='#ffffffbb'  # keyhighlight

/home/krystian/.config/i3/i3lock              \
--insidevercolor=$A   \
--ringvercolor=$A     \
\
--insidewrongcolor=$C \
--ringwrongcolor=$W   \
\
--insidecolor=$B      \
--ringcolor=$D        \
--linecolor=$B        \
--separatorcolor=$D   \
\
--textcolor=$D        \
--timecolor=$D        \
--datecolor=$D        \
--keyhlcolor=$A       \
--bshlcolor=$A        \
\
--screen 0            \
--blur 5              \
--clock               \
--indicator           \
--timestr="%H:%M:%S"  \
--datestr="%A, %m %Y" \

# --veriftext="Drinking verification can..."
# --wrongtext="Nope!"
# --textsize=20
# --modsize=10
# --timefont=comic-sans
# --datefont=monofur
# etc
